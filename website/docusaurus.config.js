// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Katabooks',
  tagline: 'SOCBOOKs by Purple Team',
  favicon: 'img/favicon.ico',
  noIndex: true,
  url: 'https://katatau.gitlab.io',
  baseUrl: '/katabooks',
  organizationName: 'katatau',
  projectName: 'katabooks',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  i18n: {
    defaultLocale: 'en',
    locales: ['en','pt-br'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          breadcrumbs: true,
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: ({versionDocsDirPath, docPath}) =>
            `https://gitlab.com/katatau/katabooks/-/edit/main/website/${versionDocsDirPath}/${docPath}`,
        },
        blog: {
          showReadingTime: true,
          editUrl: ({versionDocsDirPath, docPath}) =>
            `https://gitlab.com/katatau/katabooks/-/edit/main/website/${versionDocsDirPath}/${docPath}`,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      docs: {
        sidebar: {
          hideable: true,
          autoCollapseCategories: true,
        },
      },
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'SOCBOOK',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'attckpbksSidebar',
            position: 'left',
            label: 'ATT&CK Playbooks',
          },
          {to: '/blog', label: 'Purple Blog', position: 'left'},
          {
            href: 'https://gitlab.com/katatau/katabooks',
            label: 'Gitlab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Playbooks',
            items: [
              {
                label: 'PurpleBooks',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Comunidade TSI',
            items: [
              {
                label: 'Stack Overflow',
                href: 'https://stackoverflow.com/questions/tagged/docusaurus',
              },
              {
                label: 'Discord',
                href: 'https://discordapp.com/invite/docusaurus',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/docusaurus',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'Gitlab',
                href: 'https://gitlab.com/katatau/katabooks',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} TSI CSIRT, made by Purple Team.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
