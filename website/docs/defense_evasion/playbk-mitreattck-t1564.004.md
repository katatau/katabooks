---
id: playbk-mitreattck-T1564_004
title: NTFS File Attributes
hide_title: false
hide_table_of_contents: false
sidebar_position: 1
pagination_label: T1564_004
custom_edit_url: https://github.com/facebook/docusaurus/edit/main/docs/api-doc-markdown.md
description: Playbook para resposta a incidentes semelhantes a técnica "T1564.004" 
keywords:
  - mitreattck.defense_evasion
  - mitreattck.T1564
slug: T1564_004
last_update:
  date: 1/1/2000
  author: Gabriel Guedes
---

# Hide Artifacts: NTFS File Attributes 

- a
- b
- c 
