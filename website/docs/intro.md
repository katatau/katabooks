---
sidebar_position: 1
---

# Front matter template (playbooks)

```{markdown}
---
id: playbk-mitreattck-t1564.004
title: NTFS File Attributes
hide_title: false
hide_table_of_contents: false
sidebar_position: 1
pagination_label: T1564.004
custom_edit_url: https://github.com/facebook/docusaurus/edit/main/docs/api-doc-markdown.md
description: Playbook para resposta a incidentes semelhantes a técnica "T1564.004" 
keywords:
  - mitreattck.defense_evasion
  - mitreattck.t1564
slug: T1564.004
last_update:
  date: 1/1/2000
  author: Gabriel Guedes
---
```
